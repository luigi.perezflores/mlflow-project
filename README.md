# MLflow Project

This a project to register ml models into MLflow and deploy it into a app using FastAPI.

## 01 Description

1. Jupyter Notebooks shows the creationg of the ML model and register model in MLflow
2. FastAPI to deploy the model into production.

## 02 Requirements

- Python: 3.11.4
- Install requirements.txt

## 03 MLflow

Model registry in [Dagshub MLflow](https://dagshub.com/luigi.perezflores/mlflow-project.mlflow/#/experiments/0?searchFilter=&orderByKey=attributes.start_time&orderByAsc=false&startTime=ALL&lifecycleFilter=Active&modelVersionFilter=All+Runs&datasetsFilter=W10%3D)


## 04 Deployment

1. Run in terminal:
        
        uvicorn main:app --reload

## APP Example:

![captura_app](./images/captura_app.png)